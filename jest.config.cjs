module.exports = {
    testEnvironment: "jsdom",
    transform: {
        "^.+.tsx?$": "ts-jest",
    },
    collectCoverage: true,
    collectCoverageFrom: [
        "./src/components/**",
        "./src/pages/**",
        "./src/services/**",
    ],
    setupFilesAfterEnv: ["@testing-library/jest-dom/extend-expect"],
    moduleNameMapper: {
        // stub out CSS imports per Jest's recommendation
        "\\.(css)$": "identity-obj-proxy",
    },
};
