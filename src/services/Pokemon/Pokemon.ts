import axios from "axios";

export const getPokemons = async (url: string | null) => {
    return (
        url &&
        axios.get(url).then((res) => {
            const pokemons = res.data.results.map((item: { url: string }) => {
                return axios.get(item.url).then((subRes) => {
                    return subRes.data;
                });
            });

            return {
                pokemons: pokemons,
                previous: res.data.previous,
                next: res.data.next,
            };
        })
    );
};
