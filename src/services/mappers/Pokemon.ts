export const PokemonMapper = (pokemons: any[]) => {
    return pokemons.map((pokemon) => {
        return {
            name: pokemon.name,
            image: pokemon.sprites.other["official-artwork"]["front_default"],
            types: TypesMapper(pokemon.types),
            stats: StatsMapper(pokemon.stats),
        };
    });
};

export const ReturnPokemon = (pokemon: any) => {
    return {
        name: pokemon.name,
        image: pokemon.sprites.other["official-artwork"]["front_default"],
        types: TypesMapper(pokemon.types),
        stats: StatsMapper(pokemon.stats),
    };
};

const TypesMapper = (types: any[]) => {
    return types.map((type) => {
        return type.type.name;
    });
};

const StatsMapper = (stats: any[]) => {
    return stats.map((stat) => {
        return {
            name: stat.stat.name,
            stat: stat["base_stat"],
        };
    });
};
