import React, { createContext } from "react";
import { Pokemon } from "../interfaces/interfaces";

interface Context {
    pokemon: Pokemon | null;
    setPokemon: (pokemon: Pokemon) => void;
    pokemonList: Pokemon[] | null;
    setPokemonList: (pokemons: Pokemon[]) => void;
}

export const PokedexContext = createContext<Context>({
    pokemon: null,
    setPokemon: (pokemon: Pokemon) => {},
    pokemonList: null,
    setPokemonList: (pokemons: Pokemon[]) => {},
});
