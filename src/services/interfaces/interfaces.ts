export interface Pokemon {
    name: string;
    image: string;
    types: string[];
    stats: Stat[];
}

export interface Stat {
    name: string;
    stat: number;
}

export interface IsLoading {
    bottom: boolean;
    search: boolean;
}
