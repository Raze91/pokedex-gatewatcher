import TypesPins from "./typesPins";

describe("TypesPins function", () => {
    it("should return the correct color based on the type he receives", () => {
        expect(TypesPins("normal")).toBe("#ADA494");
        expect(TypesPins("fighting")).toBe("#A45139");
        expect(TypesPins("flying")).toBe("#9CACF6");
        expect(TypesPins("poison")).toBe("#B45AA4");
        expect(TypesPins("ground")).toBe("#D6B55A");
        expect(TypesPins("rock")).toBe("#BDA55A");
        expect(TypesPins("bug")).toBe("#ADBC21");
        expect(TypesPins("ghost")).toBe("#6363B5");
        expect(TypesPins("steel")).toBe("#ADADC6");
        expect(TypesPins("grass")).toBe("#7BCE52");
        expect(TypesPins("fire")).toBe("#F75131");
        expect(TypesPins("water")).toBe("#399CFF");
        expect(TypesPins("electric")).toBe("#FFC631");
        expect(TypesPins("psychic")).toBe("#FF72A5");
        expect(TypesPins("ice")).toBe("#5ACDE7");
        expect(TypesPins("dragon")).toBe("#8858F5");
        expect(TypesPins("dark")).toBe("#735A4A");
        expect(TypesPins("fairy")).toBe("#E09AE3");
    });

    it("should return an empty string if no type is provided", () => {
        expect(TypesPins("")).toBe("");
    })
});
