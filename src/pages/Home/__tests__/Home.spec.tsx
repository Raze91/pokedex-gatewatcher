import Home from "../Home";
import { PokedexContext } from "../../../services/contexts/pokedexContext";
import { render, screen } from "@testing-library/react";

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
    ...(jest.requireActual("react-router-dom") as any),
    useNavigate: () => mockedUsedNavigate,
}));

const pokemonsMock = [
    {
        name: "pikachu",
        image: "test",
        types: ["electric"],
        stats: [
            { name: "hp", stat: 35 },
            { name: "attack", stat: 55 },
            { name: "defense", stat: 40 },
            { name: "special-attack", stat: 50 },
            { name: "special-defense", stat: 50 },
            { name: "speed", stat: 90 },
        ],
    },
    {
        name: "charmander",
        image: "test",
        types: ["fire"],
        stats: [
            { name: "hp", stat: 35 },
            { name: "attack", stat: 55 },
            { name: "defense", stat: 40 },
            { name: "special-attack", stat: 50 },
            { name: "special-defense", stat: 50 },
            { name: "speed", stat: 90 },
        ],
    },
    {
        name: "squirtle",
        image: "test",
        types: ["water"],
        stats: [
            { name: "hp", stat: 35 },
            { name: "attack", stat: 55 },
            { name: "defense", stat: 40 },
            { name: "special-attack", stat: 50 },
            { name: "special-defense", stat: 50 },
            { name: "speed", stat: 90 },
        ],
    },
];

const HomeMock = () => {
    return (
        <PokedexContext.Provider
            value={{
                pokemon: null,
                setPokemon: jest.fn(),
                pokemonList: pokemonsMock,
                setPokemonList: jest.fn(),
            }}
        >
            <Home />
        </PokedexContext.Provider>
    );
};

describe("Home should", () => {
    it("should have the right numbers of items", () => {
        render(<HomeMock />);

        expect(screen.getAllByRole("article").length).toBe(3);
    });

    xit("should add the new items with the infinite scroll", () => {
        window.scroll(0, document.documentElement.clientHeight);

        // simule le fetch des nouveaux éléments et vérifie qu'ils sont ajoutés aux éléments initials
    });

    xit("should display the loader after a search", () => {
        // vérifier que le loader s'affiche correctement après une recherche
    });

    xit("should display the search result", () => {
        // vérifier que le résultat de la recherche s'affiche
    });
});
