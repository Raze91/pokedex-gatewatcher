import styles from "./Home.module.css";
import React, { useEffect, useState, useContext } from "react";
import PokeCard from "../../components/PokeCard/PokeCard";
import Navbar from "../../components/Navbar/Navbar";
import Loader from "../../components/Loader/Loader";

import { POKEMON_API } from "../../services/apis/apis";
import { PokemonMapper } from "../../services/mappers/Pokemon";
import { getPokemons } from "../../services/Pokemon/Pokemon";
import { Pokemon, IsLoading} from "../../services/interfaces/interfaces";
import { PokedexContext } from "../../services/contexts/pokedexContext";

const Home = () => {
    const { pokemonList, setPokemonList } = useContext(PokedexContext);
    const [nextUrl, setNextUrl] = useState<string | null>(null);
    const [searchResult, setSearchResult] = useState<Pokemon | null>(null);
    const [isLoading, setIsLoading] = useState<IsLoading>({
        bottom: false,
        search: false,
    });

    useEffect(() => {
        getPokemons(POKEMON_API).then((res) => {
            res &&
                Promise.all(res.pokemons).then((pokemons) => {
                    setPokemonList(PokemonMapper(pokemons));
                    setNextUrl(res.next);
                });
        });
    }, []);

    useEffect(() => {
        window.addEventListener("scroll", onScroll);
        return () => window.removeEventListener("scroll", onScroll);
    }, [pokemonList]);

    const onScroll = () => {
        const scrollTop = document.documentElement.scrollTop;
        const scrollHeight = document.documentElement.scrollHeight;
        const clientHeight = document.documentElement.clientHeight;
        if (
            isLoading.bottom === false &&
            scrollTop + clientHeight >= scrollHeight
        ) {
            nextUrl && handleNextUrl();
        }
    };

    const handleNextUrl = () => {
        setIsLoading({ ...isLoading, bottom: true });
        getPokemons(nextUrl).then((res) => {
            res &&
                pokemonList &&
                Promise.all(res.pokemons).then((pokemons) => {
                    setPokemonList([
                        ...pokemonList,
                        ...PokemonMapper(pokemons),
                    ]);
                    setNextUrl(res.next);
                    setIsLoading({ ...isLoading, bottom: false });
                });
        });
    };

    if (!pokemonList) return <Loader />;

    return (
        <main>
            <Navbar
                setSearchResult={setSearchResult}
                setIsLoading={setIsLoading}
                isLoading={isLoading}
            />
            <section className={styles.pokemonsCtnr}>
                {isLoading.search ? (
                    <Loader />
                ) : (
                    <>
                        {searchResult ? (
                            <>
                                {typeof searchResult === "string" ? (
                                    <p>{searchResult}</p>
                                ) : (
                                    <PokeCard pokemon={searchResult} />
                                )}
                            </>
                        ) : (
                            <>
                                {pokemonList?.map((pokemon: Pokemon, key) => (
                                    <PokeCard pokemon={pokemon} key={key} />
                                ))}
                            </>
                        )}
                    </>
                )}
            </section>
        </main>
    );
};

export default Home;
