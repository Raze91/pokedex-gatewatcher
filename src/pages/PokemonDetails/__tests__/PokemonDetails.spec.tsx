import PokemonDetails from "../PokemonDetails";
import { PokedexContext } from "../../../services/contexts/pokedexContext";
import { render, screen } from "@testing-library/react";

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
    ...(jest.requireActual("react-router-dom") as any),
    useNavigate: () => mockedUsedNavigate,
}));

const pokemon = {
    name: "pikachu",
    image: "test",
    types: ["electric"],
    stats: [
        { name: "hp", stat: 35 },
        { name: "attack", stat: 55 },
        { name: "defense", stat: 40 },
        { name: "special-attack", stat: 50 },
        { name: "special-defense", stat: 50 },
        { name: "speed", stat: 90 },
    ],
};

const DetailsMock = () => {
    return (
        <PokedexContext.Provider
            value={{
                pokemon: pokemon,
                setPokemon: jest.fn(),
                pokemonList: null,
                setPokemonList: jest.fn(),
            }}
        >
            <PokemonDetails />
        </PokedexContext.Provider>
    );
};

describe("PokemonDetails should", () => {
    it("should exist", () => {
        render(<DetailsMock />);

        expect(screen.getByLabelText("pikachu-section")).toBeDefined();
    });

    it("should have the right image source", () => {
        render(<DetailsMock />);

        expect(screen.getByLabelText("pikachu-image")).toHaveProperty(
            "src",
            "http://localhost/test"
        );
    });

    it("should display the correct informations", () => {
        render(<DetailsMock />);

        expect(screen.getByLabelText("pikachu-title").textContent).toBe(
            "pikachu"
        );
        expect(screen.getByLabelText("pikachu-type-electric")).toBeDefined();
        expect(screen.getByLabelText("pikachu-type-electric")).toHaveStyle(
            "background-color: #FFC631;"
        );
        expect(screen.getByLabelText("pikachu-stat-hp")).toBeDefined();
        expect(screen.getByLabelText("pikachu-stat-attack")).toBeDefined();
        expect(screen.getByLabelText("pikachu-stat-defense")).toBeDefined();
        expect(
            screen.getByLabelText("pikachu-stat-special-attack")
        ).toBeDefined();
        expect(
            screen.getByLabelText("pikachu-stat-special-defense")
        ).toBeDefined();
        expect(screen.getByLabelText("pikachu-stat-speed")).toBeDefined();
    });
});
