import React, { useContext, useEffect } from "react";
import styles from "./PokemonDetails.module.css";
import { PokedexContext } from "../../services/contexts/pokedexContext";
import { useNavigate } from "react-router-dom";
import { Pokemon } from "../../services/interfaces/interfaces";
import TypesPins from "../../services/tools/typesPins";

const PokemonDetails = () => {
    const navigate = useNavigate();
    const { pokemon } = useContext(PokedexContext);

    useEffect(() => {
        if (!pokemon) navigate("/");
    }, [pokemon]);

    return (
        <>
            <button
                className={styles.backButton}
                onClick={() => navigate("/")}
                aria-label={`${pokemon?.name}-back-button`}
            >
                Back
            </button>
            <section
                className={styles.detailsCtnr}
                aria-label={`${pokemon?.name}-section`}
            >
                <img
                    className={styles.image}
                    src={pokemon?.image}
                    alt={pokemon?.name}
                    aria-label={`${pokemon?.name}-image`}
                />
                <div className={styles.content}>
                    <h1
                        className={styles.title}
                        aria-label={`${pokemon?.name}-title`}
                    >
                        {pokemon?.name}
                    </h1>
                    <h3>Types</h3>
                    <div
                        className={styles.types}
                        aria-label={`${pokemon?.name}-types`}
                    >
                        {pokemon?.types.map((type, key) => (
                            <span
                                key={key}
                                style={{ backgroundColor: TypesPins(type) }}
                                aria-label={`${pokemon?.name}-type-${type}`}
                            >
                                {type}
                            </span>
                        ))}
                    </div>
                    <div
                        className={styles.stats}
                        aria-label={`${pokemon?.name}-stats`}
                    >
                        <h3>Base stats :</h3>
                        <ul>
                            {pokemon?.stats.map((stat, key) => (
                                <li
                                    key={key}
                                    aria-label={`${pokemon?.name}-stat-${stat.name}`}
                                >
                                    {stat.name} : {stat.stat}
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </section>
        </>
    );
};

export default PokemonDetails;
