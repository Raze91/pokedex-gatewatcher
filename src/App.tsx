import React, { useState } from "react";
import { PokedexContext } from "./services/contexts/pokedexContext";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./pages/Home/Home";
import PokemonDetails from "./pages/PokemonDetails/PokemonDetails";
import { Pokemon } from "./services/interfaces/interfaces";

function App() {
    const [pokemonList, setPokemonList] = useState<Pokemon[] | null>(null);
    const [pokemon, setPokemon] = useState<Pokemon | null>(null);
    return (
        <PokedexContext.Provider
            value={{
                pokemon: pokemon,
                setPokemon: setPokemon,
                pokemonList: pokemonList,
                setPokemonList: setPokemonList,
            }}
        >
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/:pokemonName" element={<PokemonDetails />} />
                </Routes>
            </BrowserRouter>
        </PokedexContext.Provider>
    );
}

export default App;
