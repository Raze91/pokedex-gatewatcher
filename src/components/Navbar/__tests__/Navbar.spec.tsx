import Navbar from "../Navbar";
import userEvent from "@testing-library/user-event";
import { PokedexContext } from "../../../services/contexts/pokedexContext";
import { render, screen } from "@testing-library/react";

const NavMock = () => {
    return (
        <PokedexContext.Provider
            value={{
                pokemon: null,
                setPokemon: jest.fn(),
                pokemonList: null,
                setPokemonList: jest.fn(),
            }}
        >
            <Navbar
                setSearchResult={jest.fn()}
                setIsLoading={jest.fn()}
                isLoading={{ bottom: false, search: false }}
            />
        </PokedexContext.Provider>
    );
};

describe("Navbar", () => {
    it("should display correctly", () => {
        render(<NavMock />);

        expect(screen.getByLabelText("global-title")).toBeDefined();
        expect(screen.getByLabelText("search-input")).toBeDefined();
        expect(screen.getByLabelText("search-submit-button")).toBeDefined();
    });

    xit("should be able to change the value of the input", () => {
        // test pour simuler le changement de la valeur de l'input
        render(<NavMock />);

        const searchInput = screen.getByLabelText("search-input");

        expect(searchInput).toHaveValue("");
        userEvent.type(searchInput, "charmander");
        expect(searchInput).toHaveValue("charmander");
    });
});
