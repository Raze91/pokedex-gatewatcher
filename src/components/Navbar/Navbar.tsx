import "./Navbar.module.css";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { ReturnPokemon } from "../../services/mappers/Pokemon";
import { Pokemon, IsLoading } from "../../services/interfaces/interfaces";

interface NavbarProps {
    setSearchResult: (searchResult: Pokemon | null) => void;
    setIsLoading: (IsLoading: IsLoading) => void;
    isLoading: IsLoading;
}

const Navbar = ({ setSearchResult, setIsLoading, isLoading }: NavbarProps) => {
    const [search, setSearch] = useState<string>("");

    useEffect(() => {
        if (search.length === 0) {
            setSearchResult(null);
        }
    }, [search]);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let { value } = e.target;

        setSearch(value);
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setIsLoading({ ...isLoading, search: true });
        axios
            .get(`https://pokeapi.co/api/v2/pokemon/${search}`)
            .then((res) => {
                setSearchResult(ReturnPokemon(res.data));
                setIsLoading({ ...isLoading, search: false });
            })
            .catch((err) => {
                setSearchResult(err.response.data);
                setIsLoading({ ...isLoading, search: false });
            });
    };

    return (
        <nav>
            <h1 aria-label="global-title">Pokedex</h1>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input
                    type="text"
                    value={search}
                    onChange={(e) => handleChange(e)}
                    aria-label="search-input"
                />
                <button type="submit" aria-label="search-submit-button" disabled={search.length === 0}>
                    Search
                </button>
            </form>
        </nav>
    );
};

export default Navbar;
