import { render, screen } from "@testing-library/react";
import fireEvent from "@testing-library/user-event";
import { PokedexContext } from "../../../services/contexts/pokedexContext";
import PokeCard from "../PokeCard";

const pokemonMock = {
    name: "pikachu",
    image: "test",
    stats: [{ name: "hp", stat: 60 }],
    types: ["electric"],
};

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
    ...(jest.requireActual("react-router-dom") as any),
    useNavigate: () => mockedUsedNavigate,
}));

const PokeCardMock = () => {
    return (
        <PokedexContext.Provider
            value={{
                pokemon: null,
                setPokemon: jest.fn(),
                pokemonList: null,
                setPokemonList: jest.fn(),
            }}
        >
            <PokeCard pokemon={pokemonMock} />
        </PokedexContext.Provider>
    );
};

describe("PokeCard should", () => {
    it("exist", () => {
        render(<PokeCardMock />);

        expect(screen.getByRole("article")).toBeDefined();
        expect(screen.getByLabelText("pikachu-card")).toBeDefined();
    });

    it("have the correct image source", () => {
        render(<PokeCardMock />);

        expect(screen.getByLabelText("pikachu-image")).toHaveProperty(
            "src",
            "http://localhost/test"
        );
    });

    it("display the correct informations", () => {
        render(<PokeCardMock />);

        expect(screen.getByLabelText("pikachu-title").textContent).toBe(
            "pikachu"
        );
        expect(screen.getByLabelText("pikachu-type-electric")).toBeDefined();
        expect(screen.getByLabelText("pikachu-type-electric")).toHaveStyle(
            "background-color: #FFC631;"
        );
    });
});
