import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Pokemon } from "../../services/interfaces/interfaces";
import TypesPins from "../../services/tools/typesPins";
import { PokedexContext } from "../../services/contexts/pokedexContext";
import styles from "./PokeCard.module.css";

const PokeCard = ({ pokemon }: { pokemon: Pokemon }) => {
    const navigate = useNavigate();
    const { setPokemon } = useContext(PokedexContext);

    const handleClick = () => {
        setPokemon(pokemon);
        navigate(`/${pokemon.name}`);
    };

    return (
        <div
            className={styles.card}
            role="article"
            aria-label={`${pokemon.name}-card`}
            onClick={() => handleClick()}
        >
            <img src={pokemon.image} aria-label={`${pokemon.name}-image`} />
            <div className={styles.infos}>
                <h3
                    className={styles.title}
                    aria-label={`${pokemon.name}-title`}
                >
                    {pokemon.name}
                </h3>
                <div
                    className={styles.types}
                    aria-label={`${pokemon.name}-types`}
                >
                    {pokemon.types.map((type, key) => (
                        <span
                            key={key}
                            style={{ backgroundColor: TypesPins(type) }}
                            aria-label={`${pokemon.name}-type-${type}`}
                        >
                            {type}
                        </span>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default PokeCard;
