import Loader from "../Loader";
import { PokedexContext } from "../../../services/contexts/pokedexContext";
import { render, screen } from '@testing-library/react';

const LoaderMock = () => {
    return (
        <PokedexContext.Provider
            value={{
                pokemon: null,
                setPokemon: jest.fn(),
                pokemonList: null,
                setPokemonList: jest.fn(),
            }}
        >
            <Loader />
        </PokedexContext.Provider>
    );
};


describe("Loader", () => {
    it('should display correctly', () => {
        render(<LoaderMock />);

        expect(screen.getByLabelText('pokeball-loader')).toBeDefined();
    })
})